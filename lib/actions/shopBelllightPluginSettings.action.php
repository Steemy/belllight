<?php

/**
 * Класс получения настроек бекенд
 *
 * @author Steemy, created by 23.03.2018
 * @link http://steemy.ru/
 */

class shopBelllightPluginSettingsAction extends waViewAction
{
    public function execute()
    {
        $pluginSetting = shopBelllightPluginSettings::getInstance();

        $settings = $pluginSetting->getSettings();
        $pluginSetting->getSettingsCheck($settings);
        $pluginSetting->addFileSetting($settings);

        $v = '';
        for($i = 0; $i < 10; $i++) {
            $v .= mt_rand(0, 9);
        }
        $settings['version'] = $v;

        $this->view->assign("settings", $settings);
    }
}
